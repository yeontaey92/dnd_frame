import { makeAutoObservable } from "mobx";


class PageListStore {

    optionsList = [
        'login',
        'payment',
        'intro',
        'member'
    ]

    constructor() {
        makeAutoObservable(this);
    }


}


export default new PageListStore();