import React, {useState, useEffect, useRef} from 'react';


export const useSetComponent = (value) => {

    const [val, getVal] = useState('');
    const [arr, setArr] = useState([]);
    let iptRef = useRef(null);

    return [arr, setArr, val, getVal, iptRef]
}

