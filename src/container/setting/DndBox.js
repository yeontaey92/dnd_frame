import React, {useEffect, useMemo, useState} from 'react';
import {useSetComponent} from "../../hooks/useSetComponent";
import _ from 'lodash';
import { pageListStore } from "../../store";



const style = {
    display: 'inline-block',
    marginRight: '20px',
    minWidth: '210px',
    border: '1px solid #909090',
    verticalAlign: 'top',
    height: 'auto',
}

const style2 = {
    display: 'flex',
    justifyContent: 'space-between',
    padding: '20px 10px',
    height: 'auto',
    fontSize: '15px',
}

const DndBox = ({ arr }) => {

    const Page = _.map(arr, (_cur, idx) =>
        <div className={'dragdrop_wrapper'} style={style} key={`list${idx}`}>
            <div style={style2} className={'page_title'}>
                <span className={'ttl'}>{_cur}</span>
                {/*<button type='button'>수정</button>*/}
                <button type='button'>추가</button>
            </div>
        </div>
    );


    return (
        Page
    );
}




export default DndBox