/*eslint-disable*/
import React, {useState} from 'react';
import { observer } from "mobx-react";
import { useSetComponent } from "../../hooks/useSetComponent";
import styles from './settings.module.css';
import DndBox from "./DndBox";
import {pageListStore} from "../../store";

const SettingContainer = observer(() => {

    const [arr, setArr, val, getVal, iptRef] = useSetComponent()



    return (
        <div className={'container_wrap'}>
            <div className={'inner'}>
                <div className={'header'}></div>
                <div className={'contents'}>
                    <div className={'wrapper'}>
                        <div className={styles['dnd_container']}>
                            <DndBox arr={arr} />
                        </div>
                        <div className={styles['dnd_input']}>
                            <div className={styles['box_wrap']}>
                                <div className="slct_box">
                                    <select ref={iptRef}>
                                        {
                                            pageListStore.optionsList.map((el, idx) => <option key={`list${idx}`} value={el}>{el}</option>)
                                        }
                                    </select>
                                </div>
                                {/*<input type='text'*/}
                                {/*       value={val}*/}
                                {/*       onChange={(e) => {*/}
                                {/*           getVal(e.target.value);*/}
                                {/*       }}*/}
                                {/*       ref={iptRef}*/}
                                {/*/>*/}
                                <button type='button'
                                        onClick={() => {
                                            iptRef.current.value.length ? setArr(prev => [...prev, iptRef.current.value]) : arr
                                            getVal('');
                                        }}
                                >생성</button>
                                <button type='button'
                                >확인</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
})

export default SettingContainer;